# Lumos
A programming language intended for me to learn transpiling. It transpiles to JavaScript, and files in Lumos are with the `.lum` extension.


## Issues
Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
